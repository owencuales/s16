console.log("HELLO WORLDSIES!");

var x = prompt("Number 1");
console.log(`The number you provided is: ${x}.`);

for (let numDec = x; numDec >= 0; numDec--){
	if(numDec > 50 && numDec % 10 === 0){
		console.log(`The number is divisible by 10. Skipping the number.`);
		continue;
	}
	if(numDec > 50 && numDec % 5 === 0){
		console.log(numDec);
	}
	if(numDec <= 50){
		console.log(`The current value is 50. Terminating the loop.`);
		break;
	} 
};

let word = "supercalifragilisticexpialidocious";
let wordie = "";

console.log(word);

for (let i = 0; i < word.length; i++){
	if (
		word[i] == "a" ||
		word[i] == "e" ||
		word[i] == "i" ||
		word[i] == "o" ||
		word[i] == "u"
		) {
		continue;
	} else {
		wordie += word[i];
	}
};

console.log(wordie);
